<?php

namespace Drupal\twig_extension_block\TwigExtension;

use Drupal\block\Entity\Block;

/**
 * Class BlockTwigExtension.
 *
 * @package Drupal\twig_extension_block
 */
class BlockTwigExtension extends \Twig_Extension {

  public function getName() {
    return 'display_block';
  }

  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('display_block', array(
        $this,
        'display_block',
      ), array('is_safe' => array('html'))),
    );
  }

  public function display_block($block_id) {
    $block = Block::load($block_id);
    if (!is_null($block)) return \Drupal::entityTypeManager()
                    ->getViewBuilder('block')
                    ->view($block);
  }
}

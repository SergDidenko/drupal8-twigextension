<?php

namespace Drupal\twig_extension_block\Controller;

use Drupal\Core\Controller\ControllerBase;

class TwigExtensionController extends ControllerBase {

  public function showContent() {
    $output = array(
      '#theme' => 'twig_extension_block',
    );
    return $output;
  }

}